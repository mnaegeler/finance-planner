docker-compose exec api yarn sequelize db:create
docker-compose exec db psql -U postgres database_development -c 'DROP SCHEMA public CASCADE; CREATE SCHEMA public; GRANT ALL ON SCHEMA public TO postgres; GRANT ALL ON SCHEMA public TO public;'
docker-compose exec db psql -U postgres database_development -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'
docker-compose restart api
docker-compose exec api node src/generators/syncDatabase.js
docker-compose exec api yarn sequelize db:seed:all
