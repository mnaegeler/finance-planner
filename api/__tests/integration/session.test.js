const test = require('ava');
const request = require('supertest');

const app = require('../../src/app');
const factory = require('../factories');

test('Should authenticate with valid credentials', async t => {
  const user = await factory.create('User', { password: '1234' });

  const response = await request(app)
    .post('/api/sessions')
    .send({
      email: user.email,
      password: '1234'
    });

  t.is(response.status, 200);
});

test('Should not authenticate with non existent user', async t => {
  const response = await request(app)
    .post('/api/sessions')
    .send({
      email: 'random@user.com',
      password: '1234'
    });

  t.is(response.status, 401);
});

test('Should not authenticate when e-mail is not provided', async t => {
  const response = await request(app)
    .post('/api/sessions')
    .send({
      email: 'random@user.com'
    });

  t.is(response.status, 401);
})

test('Should not authenticate when password is not provided', async t => {
  const response = await request(app)
    .post('/api/sessions')
    .send({
      password: '123'
    });

  t.is(response.status, 401);
})

test.serial('Should not authenticate with invalid credentials', async t => {
  const user = await factory.create('User', { password: '1234' });

  const response = await request(app)
    .post('/api/sessions')
    .send({
      email: user.email,
      password: '12'
    });

  t.is(response.status, 401);
});

test.serial('Should return JWT token when authenticated', async t => {
  const user = await factory.create('User', { password: '1234' });

  const response = await request(app)
    .post('/api/sessions')
    .send({
      email: user.email,
      password: '1234'
    });

  t.truthy(response.body.token);
});

test.serial('Should be able to access private routes when authenticated', async t => {
  const user = await factory.create('User', { password: '1234' });

  const response = await request(app)
    .get('/api')
    .set('Authorization', `Bearer ${user.generateToken()}`);

  t.is(response.status, 200);
});

test.serial('Should not be able to access private routes without JWT', async t => {
  const response = await request(app)
    .get('/api');

  t.is(response.status, 401);
});


test.serial('Should not be able to access private routes with invalid JWT', async t => {
  const response = await request(app)
    .get('/api')
    .set('Authorization', 'Bearer 123123');

  t.is(response.status, 401);
});
