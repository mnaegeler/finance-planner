const faker = require('faker');
const { factory } = require('factory-girl');
const { User, Language } = require('../src/app/models');

factory.define('User', User, {
  name: faker.name.findName(),
  email: faker.internet.email(),
  password: faker.internet.password(),
})

factory.define('Language', Language, {
  name: faker.name.findName(),
  abbr: faker.name.findName(),
})

module.exports = factory;
