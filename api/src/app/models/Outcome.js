module.exports = (sequelize, DataTypes) => {
  const Outcome = sequelize.define("Outcome", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    description: DataTypes.STRING,
    dueDate: DataTypes.DATE,
    paid: DataTypes.BOOLEAN,
    paidValue: DataTypes.DECIMAL(12, 2),
    paymentDate: DataTypes.DATE,
    value: DataTypes.DECIMAL(12, 2),
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  Outcome.associate = (models) => {
    Outcome.belongsTo(models.Category);
    Outcome.belongsTo(models.User, { as: "Owner", constraints: false });
  };

  return Outcome;
};
