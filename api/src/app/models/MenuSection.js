module.exports = (sequelize, DataTypes) => {
  const MenuSection = sequelize.define("MenuSection", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    labelKey: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  MenuSection.associate = (models) => {
    MenuSection.belongsTo(models.User, { as: "Owner", constraints: false });
  };

  return MenuSection;
};
