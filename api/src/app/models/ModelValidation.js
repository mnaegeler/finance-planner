module.exports = (sequelize, DataTypes) => {
  const ModelValidation = sequelize.define("ModelValidation", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    rule: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  ModelValidation.associate = (models) => {
    ModelValidation.belongsTo(models.User, { as: "Owner", constraints: false });
  };

  return ModelValidation;
};
