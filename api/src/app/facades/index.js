const Auth = require('./Auth')
const Permission = require('./Permission')
const Rest = require('./rest')

module.exports = {
  Auth,
  Permission,
  Rest,
}
