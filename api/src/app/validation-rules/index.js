/**
 * TODO: Como criar validações personalizadas
 */
const validations = {
}

async function makeValidations (meta, data, user, instanceId = null) {
  if (!meta.ModelValidations || !meta.ModelValidations.length) {
    return
  }

  for (let index = 0; index < meta.ModelValidations.length; index++) {
    let modelValidation = meta.ModelValidations[index]
    if (!modelValidation.rule) {
      continue
    }
    
    if (!validations[modelValidation.rule]) {
      throw new Error(`Validation function ${modelValidation.rule} not found`)
    }

    await validations[modelValidation.rule](meta, data, user, instanceId)
  }
}3

module.exports = {
  makeValidations,
}
