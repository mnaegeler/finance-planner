require('dotenv').config({
  path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env'
});

const cors = require('cors')
const express = require('express')

const AppController = function () {
  const app = express()

  if (process.env.NODE_ENV === 'development') {
    app.use(cors())
  }

  app.use(express.json({ limit: '50mb' }))
  app.use('/public', express.static('public'))
  app.use(require('./routes'))

  function getApp () {
    return app
  }

  return {
    getApp
  }
}

module.exports = AppController().getApp()
