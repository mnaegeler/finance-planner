const { performance } = require('perf_hooks')
const { Log } = require('./app/models')
const { Auth } = require('./app/facades')
const routes = require('express').Router()

const authMiddleware = require('./app/middleware/auth')
const sessionController = require('./app/controllers/sessionController')
const metadataController = require('./app/controllers/metadataController')
const magicController = require('./app/controllers/magicController')

async function logEndpoint({ req, res, next }, endpoint) {
  const perf = performance.now()
  let endpointResult;
  try {
    endpointResult = await endpoint(req, res, next)
  } catch (error) {
    console.error("--- ERROR ---");
    console.error(error);
    console.error("--- ----- ---");

    endpointResult = res.status(500).send({ message: error.message, name: error.name, stack: error.stack });
  }

  const dataLog = {
    UserId: Auth.user() ? Auth.user().id : null,
    function: endpoint.name || 'authMiddleware',
    method: req.method,
    path: req.path,
    query: req.query,
    statusCode: res.statusCode,
    modelName: req.get('Model'),
    timing: performance.now() - perf,
  }

  // Later on, add request body and responses for more descriptive logs (express-mung).
  Log.create(dataLog)

  return endpointResult
}

sessionController.store.name = 'sessionControllerStore'
routes.post('/api/sessions', (req, res, next) => logEndpoint({ req, res, next }, sessionController.store))
routes.post('/api/sendPasswordResetMail', (req, res, next) => logEndpoint({ req, res, next }, sessionController.sendPasswordResetMail))
routes.post('/api/setPassword', (req, res, next) => logEndpoint({ req, res, next }, sessionController.setPassword))

authMiddleware.name = 'authMiddleware'
routes.use((req, res, next) => logEndpoint({ req, res, next }, authMiddleware))

routes.get('/api/metadata', (req, res) => logEndpoint({ req, res }, metadataController.getMeta))

routes.get('/api', (req, res) => {
  return res.status(200).send({ message: 'Welcome to the Supernova!' })
})

routes.post('/api/changeOrganization', (req, res, next) => logEndpoint({ req, res, next }, sessionController.changeOrganization))

routes.get('/api/data', (req, res, next) => logEndpoint({ req, res, next }, magicController.browse))
routes.post('/api/data', (req, res, next) => logEndpoint({ req, res, next }, magicController.store))
routes.get('/api/data/:id', (req, res, next) => logEndpoint({ req, res, next }, magicController.show))
routes.put('/api/data/:id', (req, res, next) => logEndpoint({ req, res, next }, magicController.update))
routes.delete('/api/data/:id', (req, res, next) => logEndpoint({ req, res, next }, magicController.delete))

module.exports = routes;
