import { writable } from 'svelte/store'

let user = writable(null)
let authToken = writable(null)
let views = writable(null)
let labels = writable(null)
let currentOrganization = writable(null)
let organizations = writable(null)

export {
  user,
  authToken,
  views,
  labels,
  currentOrganization,
  organizations,
}
