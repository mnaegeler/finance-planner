import { labels } from '../store'

let $labels = {}

labels.subscribe(value => $labels = value)

function getLabel (labelKey, suppressWarn=false) {
  if (labelKey && typeof labelKey !== 'string') {
    labelKey = labelKey.labelKey
  }

  if (suppressWarn === false && (!$labels || !$labels[labelKey])) {
    // console.warn(`Label not found. Label key: ${labelKey}`)
  }

  return ($labels && $labels[labelKey]) || labelKey
}

function getPlaceholder (key, fallbackLabel=true, suppressWarn=false) {
  if (key && typeof key !== 'string') {
    if (!key.placeholder) {
      key = fallbackLabel === true ? key.labelKey : '...'
    } else {
      key = key.placeholder
    }
  }

  if (key === '...') {
    return key
  }

  if (suppressWarn === false && !$labels[key]) {
    // console.warn(`Label not found. Label key: ${key}`)
  }

  return $labels[key] || key
}

function getFilterLabel(filterField) {
  if (filterField.labelKey) {
    return getLabel(filterField)
  }

  return getLabel(filterField.ViewField)
}

function buildURL (url) {
  let host = ''

  if (Array.isArray(url)) {
    url = url[0]
  }

  if (!url) {
    return ''
  }

  const isBase64 = url.startsWith('data:')
  if (process.env.NODE_ENV === 'development' && !isBase64) {
    host = 'http://localhost:3000'
  }

  return `${host}${url}`
}

function crop (string, length) {
  return string ? string.replace(new RegExp(`^(.{0,${length}}).*$`), '$1') : ''
}

function getInitials(name) {
  return name.split(' ')
    .map(word => word[0])
    .join('')
    .toUpperCase()
}

function convertTime (time) {
  if (time !== undefined && time > 0) {
    const minutes = Math.floor(time / 60)
    const seconds = time - minutes * 60

    const hours = Math.floor(time / 3600)
    time = time - hours * 3600

    return str_pad_left(hours, '0', 2) + ':' + str_pad_left(minutes, '0', 2) + ':' + str_pad_left(seconds, '0', 2)
  }
  return 'hh:mm:ss'
}

function str_pad_left(string, pad, length) {
  return (new Array(length + 1).join(pad) + string).slice(-length)
}

export {
  getLabel,
  getPlaceholder,
  getFilterLabel,
  buildURL,
  crop,
  getInitials,
  convertTime
}
